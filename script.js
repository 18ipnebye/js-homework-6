"use strict"
// 1
let product = {
    name: "vinny",
    price: 100,
    _discount: 20,

    get discount(){
        return this._discount;
    },

    set discount(value){
        if(!isNaN(value) && value >= 0 && value <= 100){
            this._discount = value;
        }
    },
    
    totalPrice: function(){
        let newPrice = this.price - (this.price * this._discount / 100);
       return newPrice ;
    } 
};
console.log(`Total price with discount is ${product.totalPrice()}`)

// 2
let getNameAge = {
    name: null,
    age: null,
}
while (!isNaN(getNameAge.name) || getNameAge.name === " ") {
    getNameAge.name = prompt("enter your name")
}
while (!getNameAge.age) {
    getNameAge.age = +prompt("enter how old are you")
}

let showNameAge = function(){
alert(`Hi, my name is ${getNameAge.name} and I am ${getNameAge.age} years old`);
}
showNameAge()

// 3
let copyObj = (someObj)=>{
newObj = {};
for (let key in someObj){
    if(typeof key === "object"){
        for (let newKey in key){
            newObj[key][newKey] = someObj[key][newKey];
        }
    }
    newObj[key] = someObj[key];
}
return newObj
}

// Object.defineProperty(r, "newelement", {value: "12345", writable: false});
